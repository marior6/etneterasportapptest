package cz.etnetera.marekhaubert.sportapp.ui.create

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import cz.etnetera.marekhaubert.sportapp.R
import cz.etnetera.marekhaubert.sportapp.databinding.FragmentCreateBinding
import cz.etnetera.marekhaubert.sportapp.ui.base.BaseFragment
import cz.etnetera.marekhaubert.sportapp.ui.list.ListFragment
import cz.etnetera.marekhaubert.sportapp.ui.main.MainActivity

/**
 * Created by Marek Haubert on 16.01.2018.
 * Etnetera Group
 */
class CreateFragment : BaseFragment<FragmentCreateBinding, CreateMvvm.ViewModel>(), CreateMvvm.View {

    override fun getTitle(): String = getString(R.string.title_create)
    override fun getMenuItemResId(): Int = R.id.nav_create_event

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        fragmentComponent.inject(this)
        return setAndBindContentView(inflater, container, savedInstanceState, R.layout.fragment_create)
    }

    override fun itemSaved(savedSuccessfully: Boolean) {
        val stringResId = if (savedSuccessfully) R.string.item_saved_successfully else R.string.item_saved_failed
        Toast.makeText(context, stringResId, Toast.LENGTH_LONG).show()

        if (savedSuccessfully && activity is MainActivity) {
            (activity as MainActivity).replaceFragment(ListFragment())
        }
    }

    override fun emptyMandatoryField(nameFieldEmpty: Boolean) {
        if(nameFieldEmpty){
            binding.editName.error = getString(R.string.mandatory_field)
        } else {
            binding.editPlace.error = getString(R.string.mandatory_field)
        }
    }

    override fun wrongTimeFormat() {
        binding.editTime.error = getString(R.string.wrong_number_format)
    }

    override fun onResume() {
        super.onResume()
        viewModel.loadCreatedSport()
    }

    override fun onPause() {
        super.onPause()
        viewModel.saveCreatedSport()
    }
}