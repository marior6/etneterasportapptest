package cz.etnetera.marekhaubert.sportapp.data.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import cz.etnetera.marekhaubert.sportapp.util.getCurrentTimestampInSeconds

interface ISportEvent {
    var name: String?
    var place: String?
    var time: Int?
    var created: Long?
}

data class SportEvent(override var name: String? = null, override var place: String? = null, override var time: Int? = 0, override var created: Long? = getCurrentTimestampInSeconds()) : ISportEvent

@Entity data class SportEventSQL(override var name: String?, override var place: String?, override var time: Int?, override var created: Long? = getCurrentTimestampInSeconds()) : ISportEvent {
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
}