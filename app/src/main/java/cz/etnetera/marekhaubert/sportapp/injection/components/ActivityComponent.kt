package cz.etnetera.marekhaubert.sportapp.injection.components

import android.content.Context
import android.support.v4.app.FragmentManager

import cz.etnetera.marekhaubert.sportapp.injection.modules.ActivityModule
import cz.etnetera.marekhaubert.sportapp.injection.modules.ViewModelModule
import cz.etnetera.marekhaubert.sportapp.injection.qualifier.ActivityContext
import cz.etnetera.marekhaubert.sportapp.injection.qualifier.ActivityFragmentManager
import cz.etnetera.marekhaubert.sportapp.injection.scopes.PerActivity
import cz.etnetera.marekhaubert.sportapp.ui.base.navigator.Navigator
import cz.etnetera.marekhaubert.sportapp.ui.main.MainActivity

import dagger.Component

/* Copyright 2016 Patrick Löwenstein
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */
@PerActivity
@Component(dependencies = arrayOf(AppComponent::class), modules = arrayOf(ActivityModule::class, ViewModelModule::class))
interface ActivityComponent : AppComponent {

    @ActivityContext fun activityContext(): Context
    @ActivityFragmentManager fun defaultFragmentManager(): FragmentManager
    fun navigator(): Navigator

    fun inject(activity: MainActivity)

}
