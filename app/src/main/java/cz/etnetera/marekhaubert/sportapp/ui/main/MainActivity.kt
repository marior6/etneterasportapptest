package cz.etnetera.marekhaubert.sportapp.ui.main

import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.view.MenuItem
import cz.etnetera.marekhaubert.sportapp.databinding.ActivityMainBinding
import cz.etnetera.marekhaubert.sportapp.R
import cz.etnetera.marekhaubert.sportapp.ui.base.BaseActivity
import cz.etnetera.marekhaubert.sportapp.ui.base.navigator.Navigator
import cz.etnetera.marekhaubert.sportapp.ui.create.CreateFragment
import cz.etnetera.marekhaubert.sportapp.ui.list.ListFragment
import javax.inject.Inject

/**
 * Created by Marek Haubert on 16.01.2018.
 * Etnetera Group
 */
class MainActivity : BaseActivity<ActivityMainBinding, MainMvvm.ViewModel>(), MainMvvm.View, NavigationView.OnNavigationItemSelectedListener {

    @Inject lateinit var navigator: Navigator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        activityComponent.inject(this)
        setAndBindContentView(savedInstanceState, R.layout.activity_main)

        setSupportActionBar(binding.toolbar)
        initDrawer()
        binding.navView.setNavigationItemSelectedListener(this)

        if (savedInstanceState == null) {
            replaceFragment(ListFragment())
        }
    }

    fun setMenuCheckedItem(itemResId: Int) {
        binding.navView.setCheckedItem(itemResId)
    }

    private fun initDrawer() {
        val toggle = ActionBarDrawerToggle(this, binding.drawerLayout, binding.toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        binding.drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
    }

    override fun onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            R.id.nav_list -> replaceFragment(ListFragment())
            R.id.nav_create_event -> replaceFragment(CreateFragment())
        }
        binding.drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    fun replaceFragment(fragment: Fragment) {
        navigator.replaceFragment(R.id.layout_fragment_holder, fragment, null)
    }

}
