package cz.etnetera.marekhaubert.sportapp.injection.modules


import cz.etnetera.marekhaubert.sportapp.ui.create.CreateMvvm
import cz.etnetera.marekhaubert.sportapp.ui.create.CreateViewModel
import cz.etnetera.marekhaubert.sportapp.ui.list.*
import cz.etnetera.marekhaubert.sportapp.ui.main.MainMvvm
import cz.etnetera.marekhaubert.sportapp.ui.main.MainViewModel

import dagger.Binds
import dagger.Module

/* Copyright 2016 Patrick Löwenstein
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */
@Module
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindMainViewModel(viewModel: MainViewModel): MainMvvm.ViewModel

    @Binds
    internal abstract fun bindListViewModel(viewModel: ListViewModel): ListMvvm.ViewModel

    @Binds
    internal abstract fun bindCreateViewModel(viewModel: CreateViewModel): CreateMvvm.ViewModel

    @Binds
    internal abstract fun bindSportEventAdapterViewModel(viewModel: SportEventAdapterViewModel): SportEventAdapterMvvm.ViewModel

}
