package cz.etnetera.marekhaubert.sportapp.ui.main

import cz.etnetera.marekhaubert.sportapp.injection.scopes.PerActivity
import cz.etnetera.marekhaubert.sportapp.ui.base.navigator.FragmentNavigator
import cz.etnetera.marekhaubert.sportapp.ui.base.navigator.Navigator
import cz.etnetera.marekhaubert.sportapp.ui.base.viewmodel.BaseViewModel

import javax.inject.Inject

/**
 * Created by Marek on 16.01.2018.
 */
@PerActivity
class MainViewModel
@Inject
constructor(private val navigator: Navigator) : BaseViewModel<MainMvvm.View>(), MainMvvm.ViewModel {

}
