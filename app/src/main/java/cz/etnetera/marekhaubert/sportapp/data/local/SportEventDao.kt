package cz.etnetera.marekhaubert.sportapp.data.local

import android.arch.persistence.room.*
import cz.etnetera.marekhaubert.sportapp.data.model.SportEventSQL

@Dao
interface SportEventDao {

    @Query("SELECT * FROM SportEventSQL")
    fun getAllEvents(): List<SportEventSQL>

    @Insert
    fun insert(sportEventSQL: SportEventSQL);
}