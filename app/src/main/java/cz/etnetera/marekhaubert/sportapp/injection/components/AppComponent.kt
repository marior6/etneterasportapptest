package cz.etnetera.marekhaubert.sportapp.injection.components

import android.content.Context
import android.content.res.Resources
import com.google.firebase.database.FirebaseDatabase

import cz.etnetera.marekhaubert.sportapp.data.local.SportEventDao
import cz.etnetera.marekhaubert.sportapp.injection.modules.AppModule
import cz.etnetera.marekhaubert.sportapp.injection.modules.DataModule
import cz.etnetera.marekhaubert.sportapp.injection.modules.NetModule
import cz.etnetera.marekhaubert.sportapp.injection.qualifier.AppContext
import cz.etnetera.marekhaubert.sportapp.injection.scopes.PerApplication
import dagger.Component


/* Copyright 2016 Patrick Löwenstein
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */
@PerApplication
@Component(modules = arrayOf(AppModule::class, NetModule::class, DataModule::class))
interface AppComponent {
    @AppContext fun appContext(): Context
    fun resources(): Resources

    fun firebaseDatabase(): FirebaseDatabase
    fun sportEventDao(): SportEventDao
}
