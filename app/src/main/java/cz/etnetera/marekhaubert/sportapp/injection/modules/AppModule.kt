package cz.etnetera.marekhaubert.sportapp.injection.modules

import android.app.Application
import android.arch.persistence.room.Room
import android.content.Context
import android.content.res.Resources
import com.google.firebase.database.FirebaseDatabase
import cz.etnetera.marekhaubert.sportapp.App
import cz.etnetera.marekhaubert.sportapp.data.database.SportDatabase
import cz.etnetera.marekhaubert.sportapp.injection.qualifier.AppContext
import cz.etnetera.marekhaubert.sportapp.injection.scopes.PerApplication
import dagger.Module
import dagger.Provides

/* Copyright 2016 Patrick Löwenstein
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * ------
 *
 * FILE MODIFIED 2017 Tailored Media GmbH */
@Module
class AppModule(private val app: Application) {

    @Provides
    @PerApplication
    @AppContext
    internal fun provideAppContext(): Context {
        return app
    }

    @Provides
    @PerApplication
    internal fun provideResources(): Resources {
        return app.resources
    }

    @Provides
    @PerApplication
    internal fun provideFirebaseDatabase(): FirebaseDatabase = FirebaseDatabase.getInstance()

    @Provides
    @PerApplication
    fun providesSportDatabase(): SportDatabase = Room.databaseBuilder(app, SportDatabase::class.java, "etnetera-sport-db").allowMainThreadQueries().build()

    @Provides
    @PerApplication
    fun providesSportEventDao(database: SportDatabase) = database.sportEventDao()

}
