package cz.etnetera.marekhaubert.sportapp.util

/**
 * Created by Marek Haubert on 16.01.2018.
 * Etnetera Group
 */
val CREATED_SPORT_EVENT_KEY: String = "created_sport_event_key"
val CREATED_SPORT_TO_LOCAL_STORAGE_KEY: String = "created_sport_to_local_storage_key"