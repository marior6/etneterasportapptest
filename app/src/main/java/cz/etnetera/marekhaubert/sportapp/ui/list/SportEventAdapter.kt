package cz.etnetera.marekhaubert.sportapp.ui.list

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import cz.etnetera.marekhaubert.sportapp.R
import cz.etnetera.marekhaubert.sportapp.data.model.ISportEvent
import cz.etnetera.marekhaubert.sportapp.data.model.SportEvent
import cz.etnetera.marekhaubert.sportapp.databinding.ItemSportEventBinding
import cz.etnetera.marekhaubert.sportapp.injection.qualifier.AppContext
import cz.etnetera.marekhaubert.sportapp.injection.scopes.PerFragment
import cz.etnetera.marekhaubert.sportapp.injection.scopes.PerViewHolder
import cz.etnetera.marekhaubert.sportapp.ui.base.BaseViewHolder
import cz.etnetera.marekhaubert.sportapp.ui.base.view.MvvmView
import cz.etnetera.marekhaubert.sportapp.ui.base.viewmodel.BaseViewModel
import cz.etnetera.marekhaubert.sportapp.ui.base.viewmodel.MvvmViewModel
import cz.etnetera.marekhaubert.sportapp.util.Utils
import javax.inject.Inject

/**
 * Created by Marek Haubert on 16.01.2018.
 * Etnetera Group
 */
@PerFragment
class SportEventAdapter
@Inject
constructor() : RecyclerView.Adapter<SportEventAdapterViewHolder>() {

    var sportEventsList = arrayListOf<Pair<ISportEvent, Boolean>>()

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): SportEventAdapterViewHolder =
            Utils.createViewHolder(viewGroup, R.layout.item_sport_event, ::SportEventAdapterViewHolder)

    override fun onBindViewHolder(sportEventAdapterViewHolder: SportEventAdapterViewHolder, position: Int) {
        sportEventAdapterViewHolder.viewModel.update(sportEventsList[position], position == sportEventsList.size - 1)
        sportEventAdapterViewHolder.executePendingBindings()
    }

    override fun getItemCount(): Int = sportEventsList.size
}

interface SportEventAdapterMvvm {

    interface ViewModel : MvvmViewModel<MvvmView> {
        fun update(sportEventItem: Pair<ISportEvent, Boolean>, b: Boolean)

        val sportEventItem: Pair<ISportEvent, Boolean>?
    }
}

@PerViewHolder
class SportEventAdapterViewModel
@Inject
constructor(@AppContext private val context: Context) : BaseViewModel<MvvmView>(), SportEventAdapterMvvm.ViewModel {

    lateinit override var sportEventItem: Pair<ISportEvent, Boolean>

    override fun update(sportEventItem: Pair<ISportEvent, Boolean>, b: Boolean) {
        this.sportEventItem = sportEventItem
        notifyChange()
    }

}


class SportEventAdapterViewHolder(v: View) : BaseViewHolder<ItemSportEventBinding, SportEventAdapterMvvm.ViewModel>(v), MvvmView {
    init {
        viewHolderComponent.inject(this)
        bindContentView(v)
    }
}