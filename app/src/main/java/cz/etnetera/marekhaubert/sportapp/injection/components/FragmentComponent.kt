package cz.etnetera.marekhaubert.sportapp.injection.components

import cz.etnetera.marekhaubert.sportapp.injection.modules.FragmentModule
import cz.etnetera.marekhaubert.sportapp.injection.modules.ViewModelModule
import cz.etnetera.marekhaubert.sportapp.injection.scopes.PerFragment
import cz.etnetera.marekhaubert.sportapp.ui.create.CreateFragment
import cz.etnetera.marekhaubert.sportapp.ui.list.ListFragment

import dagger.Component

/* Copyright 2016 Patrick Löwenstein
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */
@PerFragment
@Component(dependencies = arrayOf(ActivityComponent::class), modules = arrayOf(FragmentModule::class, ViewModelModule::class))
interface FragmentComponent {

    fun inject(listFragment: ListFragment)
    fun inject(createFragment: CreateFragment)
}
