package cz.etnetera.marekhaubert.sportapp.ui.list

import android.content.Context
import com.androidhuman.rxfirebase2.database.dataChangesOf
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.GenericTypeIndicator
import cz.etnetera.marekhaubert.sportapp.data.local.SportEventDao
import cz.etnetera.marekhaubert.sportapp.data.model.ISportEvent
import cz.etnetera.marekhaubert.sportapp.data.model.SportEvent
import cz.etnetera.marekhaubert.sportapp.injection.qualifier.AppContext
import cz.etnetera.marekhaubert.sportapp.injection.scopes.PerFragment
import cz.etnetera.marekhaubert.sportapp.ui.base.viewmodel.BaseViewModel
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by Marek Haubert on 16.01.2018.
 * Etnetera Group
 */
@PerFragment
class ListViewModel
@Inject
constructor(@AppContext val context: Context, override val adapter: SportEventAdapter, val firebaseDatabase: FirebaseDatabase, val sportEventDao: SportEventDao) : BaseViewModel<ListMvvm.View>(), ListMvvm.ViewModel {

    override var isLoading: Boolean = true
    override var isListEmpty: Boolean = false

    private var sportEventGenericType = object : GenericTypeIndicator<Map<String, @JvmSuppressWildcards SportEvent>>() {}

    override fun loadDataFromFirebase() {
        handleLoading(true, adapter.sportEventsList.isEmpty())

        firebaseDatabase.reference.dataChangesOf(sportEventGenericType)
                .subscribe({ sportEventMap ->
                    adapter.sportEventsList.clear()
                    sportEventMap.get().values.forEach { sportEvent ->
                        adapter.sportEventsList.add(Pair(sportEvent as ISportEvent, true))
                    }
                    loadDataFromDatabase()
                }, {
                    loadDataFromDatabase()
                    Timber.e(it, "Could not load sport events")
                })
    }

    private fun loadDataFromDatabase() {
        sportEventDao.getAllEvents().forEach { sportEvent ->
            adapter.sportEventsList.add(Pair(sportEvent as ISportEvent, false))
        }
        adapter.sportEventsList.sortByDescending { listItem -> listItem.first.created }
        adapter.notifyDataSetChanged()
        handleLoading(false, adapter.sportEventsList.isEmpty())
    }

    private fun handleLoading(isLoading: Boolean, isListEmpty: Boolean) {
        this.isLoading = isLoading
        this.isListEmpty = isListEmpty
        notifyChange()
    }
}