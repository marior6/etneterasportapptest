package cz.etnetera.marekhaubert.sportapp

import android.app.Application
import android.content.res.Resources
import cz.etnetera.marekhaubert.sportapp.injection.components.AppComponent
import cz.etnetera.marekhaubert.sportapp.injection.components.DaggerAppComponent
import cz.etnetera.marekhaubert.sportapp.injection.modules.AppModule
import io.reactivex.plugins.RxJavaPlugins
import timber.log.Timber

/**
 * Created by Marek Haubert on 16.01.2018.
 * Etnetera Group
 */
class App : Application() {

    override fun onCreate() {
        super.onCreate()

        instance = this
        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .build()
        RxJavaPlugins.setErrorHandler({ Timber.e(it) })
    }

    companion object {

        lateinit var instance: App
            private set

        lateinit var appComponent: AppComponent
            private set

        val res: Resources
            get() = instance.resources
    }
}
