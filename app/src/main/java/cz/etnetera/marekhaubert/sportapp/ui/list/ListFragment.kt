package cz.etnetera.marekhaubert.sportapp.ui.list

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cz.etnetera.marekhaubert.sportapp.R
import cz.etnetera.marekhaubert.sportapp.databinding.FragmentListBinding
import cz.etnetera.marekhaubert.sportapp.ui.base.BaseFragment
import android.support.v7.widget.DividerItemDecoration
import javax.inject.Inject


/**
 * Created by Marek Haubert on 16.01.2018.
 * Etnetera Group
 */
class ListFragment : BaseFragment<FragmentListBinding, ListMvvm.ViewModel>(), ListMvvm.View {

    @Inject lateinit var adapter: SportEventAdapter

    override fun getTitle(): String = getString(R.string.title_list)
    override fun getMenuItemResId(): Int = R.id.nav_list

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        fragmentComponent.inject(this)
        return setAndBindContentView(inflater, container, savedInstanceState, R.layout.fragment_list)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAdapter()

        viewModel.loadDataFromFirebase()
    }

    private fun initAdapter() {
        val dividerItemDecoration = DividerItemDecoration(binding.recyclerView.context, LinearLayoutManager.VERTICAL)
        binding.recyclerView.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        binding.recyclerView.addItemDecoration(dividerItemDecoration)
        binding.recyclerView.adapter = adapter
    }
}