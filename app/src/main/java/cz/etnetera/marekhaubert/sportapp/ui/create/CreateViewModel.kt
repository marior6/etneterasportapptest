package cz.etnetera.marekhaubert.sportapp.ui.create

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.view.View
import com.google.firebase.database.FirebaseDatabase
import com.google.gson.Gson
import cz.etnetera.marekhaubert.sportapp.data.local.SportEventDao
import cz.etnetera.marekhaubert.sportapp.data.model.ISportEvent
import cz.etnetera.marekhaubert.sportapp.data.model.SportEvent
import cz.etnetera.marekhaubert.sportapp.data.model.SportEventSQL
import cz.etnetera.marekhaubert.sportapp.injection.qualifier.AppContext
import cz.etnetera.marekhaubert.sportapp.injection.scopes.PerFragment
import cz.etnetera.marekhaubert.sportapp.ui.base.navigator.FragmentNavigator
import cz.etnetera.marekhaubert.sportapp.ui.base.viewmodel.BaseViewModel
import cz.etnetera.marekhaubert.sportapp.util.CREATED_SPORT_EVENT_KEY
import cz.etnetera.marekhaubert.sportapp.util.CREATED_SPORT_TO_LOCAL_STORAGE_KEY
import javax.inject.Inject

/**
 * Created by Marek Haubert on 16.01.2018.
 * Etnetera Group
 */
@PerFragment
class CreateViewModel
@Inject
constructor(@AppContext context: Context, private val firebaseDatabase: FirebaseDatabase, val navigator: FragmentNavigator, val sportEventDao: SportEventDao) : BaseViewModel<CreateMvvm.View>(), CreateMvvm.ViewModel {

    override lateinit var sportEvent: SportEvent
    override var saveToLocalStorage: Boolean = false
    private val prefs: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    private val gson: Gson = Gson()

    override fun sportNameTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        sportEvent.name = s.toString()
    }

    override fun sportPlaceTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        sportEvent.place = s.toString()
    }

    override fun sportTimeTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        try {
            sportEvent.time = s.toString().toInt()
        } catch (e: NumberFormatException) {
            sportEvent.time = 0
            view?.wrongTimeFormat()
        }
    }

    override fun onLocalStorageChanged(checked: Boolean) {
        saveToLocalStorage = checked
    }

    override fun clickOnCreateEvent(v: View) {
        if (validateSportEventValues()) {
            if (saveToLocalStorage) {
                saveDataToLocalStorage()
            } else {
                saveDataToFirebase()
            }
        }
    }

    /**
     * Returns true if all validations are correct, else false.
     */
    private fun validateSportEventValues(): Boolean {
        if (sportEvent.name.isNullOrBlank()) {
            view?.emptyMandatoryField(true)
        } else if (sportEvent.place.isNullOrBlank()) {
            view?.emptyMandatoryField(false)
        } else if (sportEvent.time!! <= 0) {
            view?.wrongTimeFormat()
        } else {
            return true
        }
        return false
    }

    private fun saveDataToLocalStorage() {
        sportEventDao.insert(SportEventSQL(sportEvent.name, sportEvent.place, sportEvent.time, sportEvent.created))
        sportEventSuccessfullySaved()
    }

    private fun sportEventSuccessfullySaved() {
        prefs.edit()
                .remove(CREATED_SPORT_EVENT_KEY)
                .remove(CREATED_SPORT_TO_LOCAL_STORAGE_KEY)
                .apply()
        sportEvent = SportEvent()
        saveToLocalStorage = false
        view?.itemSaved(true)
    }

    private fun saveDataToFirebase() {
        val firebaseKey = firebaseDatabase.reference.push().key
        firebaseDatabase.reference.child(firebaseKey).setValue(sportEvent).addOnSuccessListener {
            sportEventSuccessfullySaved()
        }.addOnFailureListener {
            view?.itemSaved(false)
        }
    }

    override fun saveCreatedSport() {
        prefs.edit()
                .putString(CREATED_SPORT_EVENT_KEY, gson.toJson(sportEvent))
                .putBoolean(CREATED_SPORT_TO_LOCAL_STORAGE_KEY, saveToLocalStorage)
                .apply()
    }

    override fun loadCreatedSport() {
        val createdSportJson = prefs.getString(CREATED_SPORT_EVENT_KEY, null)
        sportEvent = if (createdSportJson != null) gson.fromJson(createdSportJson, SportEvent::class.java) else SportEvent()
        saveToLocalStorage = prefs.getBoolean(CREATED_SPORT_TO_LOCAL_STORAGE_KEY, false)
        notifyChange()
    }
}