package cz.etnetera.marekhaubert.sportapp.ui.list

import cz.etnetera.marekhaubert.sportapp.ui.base.view.MvvmView
import cz.etnetera.marekhaubert.sportapp.ui.base.viewmodel.AdapterMvvmViewModel

/**
 * Created by Marek Haubert on 16.01.2018.
 * Etnetera Group
 */
interface ListMvvm {

    interface View : MvvmView {
    }

    interface ViewModel : AdapterMvvmViewModel<View> {
        fun loadDataFromFirebase()
        var isLoading: Boolean
        var isListEmpty: Boolean
    }
}
