package cz.etnetera.marekhaubert.sportapp.ui.create

import android.widget.NumberPicker
import cz.etnetera.marekhaubert.sportapp.data.model.SportEvent
import cz.etnetera.marekhaubert.sportapp.ui.base.view.MvvmView
import cz.etnetera.marekhaubert.sportapp.ui.base.viewmodel.MvvmViewModel

/**
 * Created by Marek Haubert on 16.01.2018.
 * Etnetera Group
 */
interface CreateMvvm {

    interface View : MvvmView {
        fun itemSaved(savedSuccessfully: Boolean)
        fun wrongTimeFormat()
        fun emptyMandatoryField(nameFieldEmpty: Boolean)
    }

    interface ViewModel : MvvmViewModel<View> {
        val sportEvent: SportEvent
        var saveToLocalStorage: Boolean
        fun sportNameTextChanged(s: CharSequence, start: Int, before: Int, count: Int)
        fun sportPlaceTextChanged(s: CharSequence, start: Int, before: Int, count: Int)
        fun sportTimeTextChanged(s: CharSequence, start: Int, before: Int, count: Int)
        fun onLocalStorageChanged(checked: Boolean)
        fun clickOnCreateEvent(v: android.view.View)
        fun saveCreatedSport()
        fun loadCreatedSport()
    }
}
