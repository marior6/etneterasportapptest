package cz.etnetera.marekhaubert.sportapp.ui.main

import cz.etnetera.marekhaubert.sportapp.ui.base.BaseFragment
import cz.etnetera.marekhaubert.sportapp.ui.base.view.MvvmView
import cz.etnetera.marekhaubert.sportapp.ui.base.viewmodel.MvvmViewModel

/**
 * Created by Marek on 16.01.2018.
 */
interface MainMvvm {

    interface View : MvvmView

    interface ViewModel : MvvmViewModel<View> {
    }
}
