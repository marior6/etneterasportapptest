package cz.etnetera.marekhaubert.sportapp.data.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import cz.etnetera.marekhaubert.sportapp.data.local.SportEventDao
import cz.etnetera.marekhaubert.sportapp.data.model.SportEventSQL


/**
 * Created by Marek Haubert on 16.01.2018.
 * Etnetera Group
 */
@Database(entities = [(SportEventSQL::class)], version = 1)
abstract class SportDatabase : RoomDatabase() {

    abstract fun sportEventDao(): SportEventDao
}